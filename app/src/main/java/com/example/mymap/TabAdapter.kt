package com.example.mymap

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter


class TabAdapter(fm: FragmentManager, var fragments : ArrayList<Fragment>) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
                return fragments[position]
            }

    override fun getCount(): Int {
        return fragments.size
           }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {

            0 -> "Drivers"
            1 -> "Buses"
            2 -> "Stops"

            else -> "new tab"
        }
    }

}