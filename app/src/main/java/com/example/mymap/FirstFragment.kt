package com.example.mymap



import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import kotlinx.android.synthetic.main.fragment_first.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class FirstFragment : Fragment() {
    lateinit var db : FirebaseFirestore
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        db = FirebaseFirestore.getInstance()
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_first, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val settings = FirebaseFirestoreSettings.Builder()
            .setTimestampsInSnapshotsEnabled(true)
            .build()
        db.firestoreSettings = settings


        db.collection("drivers").get().addOnSuccessListener { documents ->

            var profiles = ArrayList<Model>()

            documents.forEach { document ->

                var dbname: String = document.data["name"] as String
                var dbphoneNo: String = document.data["phone_no"] as String

                    Log.d(TAG, "${document.id} => ${document.data}")
                //var dID : String = document.id
//                profiles.add(Model("marah", dbname, R.drawable.ic_person_black_24dp))
//               profiles.add(Model(dbname, dbphoneNo, R.drawable.ic_person_black_24dp))
//
            }
//
            mListView.adapter = MyListAdapter(context!!, profiles)
//
        }.addOnFailureListener { ex ->
            Toast.makeText(context, "FAIL", Toast.LENGTH_LONG).show()
        }


        newButton.setOnClickListener{
            val intent = Intent(context, AddIDriverActivity::class.java)
            startActivity(intent)
        }

    }


}

