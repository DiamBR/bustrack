package com.example.mymap

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_buss_recycler_view.*

class BussRecyclerView : AppCompatActivity() {

    var busses = ArrayList<Bus>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_buss_recycler_view)


        addBuses()

        // Creates a vertical Layout Manager
        recyclerView.layoutManager = LinearLayoutManager(this)

        // Access the RecyclerView Adapter and load the data into it
        recyclerView.adapter = BussAndStopAdapter(busses, this)


    }

//    TO Delete
    private fun addBuses () {
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
        busses.add(Bus(1.2,1.3,"Al Jam3a Street"))
    }

}
