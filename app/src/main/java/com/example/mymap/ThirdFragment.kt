package com.example.mymap



import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_second.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class ThirdFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_third, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var bProfiles = ArrayList<ModelStop>()

        bProfiles.add(ModelStop("Faculty of Education", "1st"))
        bProfiles.add(ModelStop("Student.com", "2ed"))
        bProfiles.add(ModelStop("Sciences Compound", "3rd"))
        bProfiles.add(ModelStop("Faculty of Engineering", "4th"))
        bProfiles.add(ModelStop("Faculty of Arts and Design", "5th"))

        mListView.adapter= MyListAdapterStop(context!!, bProfiles)


        newButton.setOnClickListener{
            val intent = Intent(context, AddStopActivity::class.java)
            startActivity(intent)
        }


    }


}
