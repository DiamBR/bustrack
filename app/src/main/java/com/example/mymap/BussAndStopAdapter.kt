package com.example.mymap

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.busandstop.view.*

class BussAndStopAdapter(val items : ArrayList<Bus>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context)!!.inflate(R.layout.busandstop,p0, false))
    }
    // Binds each Bus in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        holder?.busTextView?.text = items.get(p1).routeName
    }

    // Gets the number of Buss in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views


}

class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each Bus to
    val busTextView = view.textView
}
