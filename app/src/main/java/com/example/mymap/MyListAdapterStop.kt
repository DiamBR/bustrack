package com.example.mymap


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.raw_list_view.view.*
//import android.widget.ArrayAdapter

class MyListAdapterStop (var mCtx : Context, var profiles : ArrayList<ModelStop>)
    : BaseAdapter() {


    override fun getItem(position: Int): ModelStop {
        return profiles[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return profiles.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        val mView : View
        val viewHolder : ViewHolder

        if (convertView == null) {
            val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
            mView = layoutInflater.inflate(R.layout.raw_list_view_stops, parent, false)
            viewHolder = ViewHolder(mView.mTextViewTitle, mView.mTextViewDesc)

            mView.tag = viewHolder

        } else {

            mView = convertView
            viewHolder = convertView.tag as ViewHolder
        }

        val profile : ModelStop = getItem(position)
        viewHolder.mTextViewTitle.text = profile.name
        viewHolder.mTextSNo.text = profile.stopNo


        return mView
    }

    private class ViewHolder(var mTextViewTitle: TextView, var mTextSNo: TextView)
}
