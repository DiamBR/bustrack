package com.example.mymap

import android.content.Context
import android.os.Build
import android.support.annotation.RequiresApi
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import kotlinx.android.synthetic.main.raw_list_view.view.*
//import android.widget.ArrayAdapter

class MyListAdapter (var mCtx : Context, var profiles : ArrayList<Model>)
    : BaseAdapter() {


    override fun getItem(position: Int): Model {
        return profiles[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return profiles.size
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var mView : View
        var viewHolder : ViewHolder

        if (convertView == null) {
            var layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
            mView = layoutInflater.inflate(R.layout.raw_list_view, parent, false)
            viewHolder = ViewHolder(mView)
            mView.tag = viewHolder

        } else {

            mView = convertView
            viewHolder = convertView.tag as ViewHolder
        }

        var profile : Model = getItem(position)
        viewHolder.mView.mTextViewTitle.text = profile.name
        viewHolder.mView.mTextViewDesc.text = profile.phoneNo
        viewHolder.mView.mImageView.setImageDrawable(mCtx.resources.getDrawable(profile.photo, null))


        return mView
    }

    private class ViewHolder(var mView: View)
}
