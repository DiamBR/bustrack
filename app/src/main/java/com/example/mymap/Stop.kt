package com.example.mymap

data class Stop (val routeLat: Double, val routeLon: Double, val stopName: String)