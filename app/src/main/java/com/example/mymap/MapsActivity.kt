package com.example.mymap

import android.app.DownloadManager
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.google.android.gms.common.api.Response
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import org.json.JSONObject

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        //Setting the Starting and Ending Points
       //val originLatLng = LatLng(-34.0, 151.0)
        val originLatLng = LatLng(32.019700, 35.870673)
       val destinatioLatLng = LatLng(32.005688, 35.876095)

        val stoptwoLatLng = LatLng(32.0104,35.5223)//Student.com
        val stopthreeLatLng = LatLng(32.005,35.5223)//Al3lmeia
        val stopfourLatLng = LatLng(32.0039,35.5235)//Engineerings
        //32.019636, 35.870638 >> al tarbia (start)
        //32.005688, 35.876095 >> Art & design (End)

        //>> Engineerings

             //Adding Markers on the Point
        googleMap!!.addMarker(MarkerOptions().position(originLatLng).title("Faculty of education stop"))
        googleMap!!.addMarker(MarkerOptions().position(destinatioLatLng).title("Faculty of Arts And Design Stop"))
        googleMap!!.addMarker(MarkerOptions().position(stoptwoLatLng).title("student.com stop"))
        googleMap!!.addMarker(MarkerOptions().position(stopthreeLatLng).title("Science Compound Stop"))
        googleMap!!.addMarker(MarkerOptions().position(stopfourLatLng).title("Faculty of engineering stop"))

        //Moving and Zooming the Camera on the Start Position
        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLng, 14.5f))
        //Creating a Mutable ( Editable ) List of LatLng ( Positions )
        val path : MutableList<List<LatLng>> = ArrayList()

        //API KEY AIzaSyCEx5pXHuaEU-9m5rPJlpJ4pNaDyFxqVWk
        val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?origin=32.019700,35.870673&destination=32.005688, 35.876095&key=AIzaSyCEx5pXHuaEU-9m5rPJlpJ4pNaDyFxqVWk"



        /*val directionsRequest =*/

            //StringRequest(Request.Method.GET, urlDirections , Response.Listener<String>{
//                response ->
//            val jsonResponse = JSONObject(response)
//            val routes = jsonResponse.getJSONArray("routes")
//            val legs = routes.getJSONObject(0).getJSONArray("legs")
//            val steps = legs.getJSONObject(0).getJSONArray("steps")
//            for ( step in 0 until steps.length() ) {
//                val points = steps.getJSONObject(step).getJSONObject("polyline").getString("points")
//                path.add(PolyUtil.decode(points))
//            }
//            for (i in 0 until path.size)
//                googleMap!!.addPolyline(PolylineOptions().addAll(path[i]).color(Color.BLACK))
//        }, Response.ErrorListener {
//                error ->
//            Log.v("Error", error.localizedMessage)
//        })


        // Add a marker in Sydney and move the camera
//      val sydney = LatLng(32.019636, 35.870638)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in AlTarbeia"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))

    }
}
