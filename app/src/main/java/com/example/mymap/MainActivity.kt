package com.example.mymap


import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.net.Uri
import android.content.Intent
import android.widget.Toast
import android.content.ActivityNotFoundException
import com.google.firebase.FirebaseApp

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FirebaseApp.initializeApp(applicationContext)

       toBusesLayout.setOnClickListener{
            var intentMap = Intent(this, MapsActivity::class.java)
            startActivity(intentMap)
        }

        // CHAT BOT LAYOUT ON CLICK TAKES YOU TO FB MESSENGER
        toChatBotLayout.setOnClickListener{
            val uri = Uri.parse("fb-messenger://user/2232639640290651")
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)

            try {
                startActivity(intent)
            } catch (e: ActivityNotFoundException){
                Toast.makeText(this, "Please Install Facebook Messenger", Toast.LENGTH_LONG).show()
            }
        }

        //LOG IN LAYOUT ON CLICK TAKES YOU TO LOGIN ACTIVITY
        toLogin.setOnClickListener {
            var intentLogin = Intent(this, LoginActivity::class.java)
            startActivity(intentLogin)
        }
    }
}
