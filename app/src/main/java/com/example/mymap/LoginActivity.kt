package com.example.mymap

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.auth.User
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {
//    var db : FirebaseFirestore = FirebaseFirestore.getInstance()
    lateinit var dbAdmin : DocumentReference
    lateinit var dbDriver : CollectionReference
    lateinit var userType : String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //spinner adapter
        var type = arrayOf("Bus Driver", "Admin")
       spinner.adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item,type)

//        // spinner item click (Admin/Driver)
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                val selectedItem = parent.getItemAtPosition(position).toString()
                userType = if (selectedItem == "Admin") {
                    "Admin"
                }//if spinner admin
                else {
                    "Driver"
                }//else spinner driver
            }//onItemSelected

            override fun onNothingSelected(parent: AdapterView<*>) {
                Toast.makeText(this@LoginActivity, "Please Choose a Type", Toast.LENGTH_LONG).show()
            }
        }

       dbAdmin = FirebaseFirestore.getInstance().document("Admin/pJaAmntuY2bjaLyUVpKP")
       dbDriver  = FirebaseFirestore.getInstance().collection("drivers")

        userLogin()

        resetPass.setOnClickListener{
            var newPass= getRandomString()
                TODO()
            //Update the database
            //Send password through sms
        }

    }

    private fun userLogin() {
        button.setOnClickListener{
            //phone no
            var phoneNo = enterPN.text.toString()

            //password
            var password = enterPass.text.toString()

                if (userType == "Admin"){

                    dbAdmin.get().addOnSuccessListener { document ->


                            var dbphoneNo: String = document.data!!["phone_no"] as String
                            var dbpassword: String = document.data!!["password"] as String


                            if (dbphoneNo.equals(phoneNo) && dbpassword.equals(password)) {
                                Toast.makeText(this@LoginActivity, "Successful", Toast.LENGTH_LONG).show()
                                val intent = Intent(this@LoginActivity, AdminActivity::class.java)
                                startActivity(intent)
                            }
                            else {
                                Toast.makeText(this@LoginActivity, "Wrong Phone no or Password", Toast.LENGTH_LONG).show()
                            }

                    }.addOnFailureListener {
                        Toast.makeText(this@LoginActivity, "Not Successful", Toast.LENGTH_LONG).show()
                      }

                }

            // DRIVER LOGIN

                if (userType == "Driver") {

                    dbDriver.get().addOnSuccessListener { documents ->

                        for (document in documents)
                        {
                            var dbphoneNo: String = document.data["phone_no"] as String
                            var dbpassword: String = document.data["password"] as String

                            if (dbphoneNo.equals(phoneNo) && dbpassword.equals(password)) {
                                Toast.makeText(this@LoginActivity, "Successful", Toast.LENGTH_LONG).show()
                                val intent = Intent(this@LoginActivity, DriverProfileActivity::class.java)
                                startActivity(intent)
                            } else {
                                Toast.makeText(this@LoginActivity, "Wrong Phone no or Password", Toast.LENGTH_LONG)
                                    .show()
                            }

                        }

                    }.addOnFailureListener {
                    Toast.makeText(this@LoginActivity, "Not Successful", Toast.LENGTH_LONG).show()
                    }
                }//end if- driver

        }//button listener
    }//func end

    private fun getRandomString() : String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        var newString = ""
        for (i in 0..5)
        {   newString += allowedChars[Math.floor(Math.random() * allowedChars.length).toInt()] }
        return newString
    }
}//end end
