package com.example.mymap

data class Admin(val buss: ArrayList<Bus>, val stops: ArrayList<Stop>) {

    fun addToBuss (bus: Bus) {
        buss.add(bus)
    }

    fun addToStop (stop: Stop) {
        stops.add(stop)
    }

    fun removeBus (bus: Bus){
        buss.remove(bus)
    }

    fun removeStop (stop: Stop) {
        stops.remove(stop)
    }

}