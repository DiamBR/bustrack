package com.example.mymap

data class Bus (val routeLat: Double, val routeLon: Double, val routeName: String)