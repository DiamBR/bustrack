package com.example.mymap


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kotlinx.android.synthetic.main.raw_list_view_bus.view.*
//import android.widget.ArrayAdapter

class MyListAdapterBus (var mCtx : Context, var profiles : ArrayList<ModelBus>)
    : BaseAdapter() {


    override fun getItem(position: Int): ModelBus {
        return profiles[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
       return profiles.size
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        val mView : View
        val viewHolder : ViewHolder

        if (convertView == null) {
            val layoutInflater : LayoutInflater = LayoutInflater.from(mCtx)
            mView = layoutInflater.inflate(R.layout.raw_list_view_bus, parent, false)
            viewHolder = ViewHolder(mView.mTextViewBusNo)

            mView.tag = viewHolder

        } else {

            mView = convertView
            viewHolder = convertView.tag as ViewHolder
        }

        val profile : ModelBus = getItem(position)
        viewHolder.mTextViewBusNo.text = profile.busNo


        return mView
    }

    private class ViewHolder(var mTextViewBusNo: TextView)
}
