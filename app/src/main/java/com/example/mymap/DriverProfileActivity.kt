package com.example.mymap


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.annotation.TargetApi
import android.app.Activity
import android.content.ContentUris
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Build
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.widget.ImageView
import android.view.MenuItem
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_driver_profile.*
import kotlinx.android.synthetic.main.app_bar_driver_profile.*
import kotlinx.android.synthetic.main.toggleswitch.*



class DriverProfileActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
//    OnMapReadyCallback



//  private lateinit var mMap: GoogleMap

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_profile)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
            this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//
//        val mapFragment = supportFragmentManager
//            .findFragmentById(R.id.map) as SupportMapFragment
//        mapFragment.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
//    override fun onMapReady(googleMap: GoogleMap) {
//        mMap = googleMap
//
//        //Setting the Starting and Ending Points
//        //val originLatLng = LatLng(-34.0, 151.0)
//        val originLatLng = LatLng(32.019636, 35.870638)
//        val destinatioLatLng = LatLng(32.005688, 35.876095)
//
//        //32.019636, 35.870638 >> al tarbia (start)
//        //32.005688, 35.876095 >> Art & design (End)
//        // 32.0039, 35.5230>> Al3lmeia
//        //32.0104, 35.5223>>Student.com
//        //32.0039, 35.5253 >> Engineering
//        //  val originLatLng = LatLng(32.019636, 35.870638)
//        //   val destinatioLatLng = LatLng(-30.0, 150.0)
//
//        //Adding Markers on the Point
//        googleMap!!.addMarker(MarkerOptions().position(originLatLng).title("Current Position"))
//        googleMap!!.addMarker(MarkerOptions().position(destinatioLatLng).title("Desired position"))
//
//        //Moving and Zooming the Camera on the Start Position
//        googleMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(originLatLng, 14.5f))
//
//        val urlDirections = "https://maps.googleapis.com/maps/api/directions/json?origin=-34.0,151.0&destination=30.0,150.0&key= AIzaSyCEx5pXHuaEU-9m5rPJlpJ4pNaDyFxqVWk"
//
//        //Creating a Mutable ( Editable ) List of LatLng ( Positions )
//        val path : MutableList<List<LatLng>> = ArrayList()
//
//        // Add a marker in Sydney and move the camera
//        val sydney = LatLng(32.019636, 35.870638)
//        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in AlTarbeia"))
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
//
//    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {

                TODO() //ADD TO DB
            }
            R.id.nav_gps_off -> {
                return false
            }
            R.id.nav_password -> {
                sendSMS()
            }

        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun gpsLocationSwitch() {

        gps_switch.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) { TODO()} else { TODO() }
        }
    }

    private fun getRandomString() : String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
        var newString = ""
            for (i in 0..31)
            {   newString += allowedChars[Math.floor(Math.random() * allowedChars.length).toInt()] }
        return newString
    }
    private fun sendSMS() {

        var newPassword = getRandomString()
        val uri = Uri.parse("smsto:0795316858")
        val intent = Intent(Intent.ACTION_SEND, uri)
        intent.putExtra("sms_body", "your new password $newPassword")
        startActivity(intent)

        TODO() //ADD TO DB
    }

}
