package com.example.mymap

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import kotlinx.android.synthetic.main.activity_admin.*

class AdminActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_admin)

        var fragment = ArrayList<Fragment>()
        fragment.add(FirstFragment())
        fragment.add(SecondFragment())
        fragment.add(ThirdFragment())

        var fragmentPagerAdapter = TabAdapter(supportFragmentManager, fragment)
        mViewPager.adapter = fragmentPagerAdapter
        mTabLayout.setupWithViewPager(mViewPager)

    }
}
